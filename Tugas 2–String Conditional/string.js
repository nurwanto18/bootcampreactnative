// Soal no 1
var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';

var result = word.concat(" "+second.concat(" "+third.concat(" "+fourth.concat(" "+fifth.concat(" " + sixth.concat(" "+seventh))))));
console.log("====Soal 1====");
console.log(result);
console.log("");


// Soal no 2
var sentence = "I am going to be React Native Developer"; 

var exampleFirstWord = sentence[0] ; 
var exampleSecondWord = sentence[2] + sentence[3]  ; 
var thirdWord = sentence.charAt(5)+sentence.charAt(6)+sentence.charAt(7)+sentence.charAt(8)+sentence.charAt(9); // lakukan sendiri 
var fourthWord = sentence[11] + sentence[12]; // lakukan sendiri 
var fifthWord = sentence[14] + sentence[15]; // lakukan sendiri 
var sixthWord = sentence.charAt(17)+sentence.charAt(18)+sentence.charAt(19)+sentence.charAt(20)+sentence.charAt(21); // lakukan sendiri 
var seventhWord = sentence[23] + sentence[24] + sentence[25] + sentence[26] + sentence[27] + sentence[28]; // lakukan sendiri 
var eighthWord = sentence[30] + sentence[31] + sentence[32] + sentence[33] + sentence[34] + sentence[35]+ sentence[36] + sentence[37]+ sentence[38]; // lakukan sendiri 

console.log("====Soal 2====");
console.log('First Word: ' + exampleFirstWord); 
console.log('Second Word: ' + exampleSecondWord); 
console.log('Third Word: ' + thirdWord); 
console.log('Fourth Word: ' + fourthWord); 
console.log('Fifth Word: ' + fifthWord); 
console.log('Sixth Word: ' + sixthWord); 
console.log('Seventh Word: ' + seventhWord); 
console.log('Eighth Word: ' + eighthWord);

console.log("");

// Soal no 3

var sentence2 = 'wow JavaScript is so cool'; 

var exampleFirstWord2 = sentence2.substring(0, 3); 
var secondWord2 = sentence2.substring(4, 14); // do your own! 
var thirdWord2 = sentence2.substring(15, 17); // do your own! 
var fourthWord2 = sentence2.substring(18, 20); // do your own! 
var fifthWord2 = sentence2.substring(21, 25); // do your own! 

console.log("====Soal 3====");
console.log('First Word: ' + exampleFirstWord2); 
console.log('Second Word: ' + secondWord2); 
console.log('Third Word: ' + thirdWord2); 
console.log('Fourth Word: ' + fourthWord2); 
console.log('Fifth Word: ' + fifthWord2);

console.log("");


//Soal no 4

var sentence3 = 'wow JavaScript is so cool'; 

var exampleFirstWord3 = sentence3.substring(0, 3); 
var secondWord3 = sentence2.substring(4, 14); // do your own! 
var thirdWord3 = sentence2.substring(15, 17); // do your own! 
var fourthWord3 = sentence2.substring(18, 20); // do your own! 
var fifthWord3 = sentence2.substring(21, 25); // do your own! 

var firstWordLength = exampleFirstWord3.length;
// lanjutkan buat variable lagi di bawah ini 
var secondWordLength = secondWord3.length;
var thirdWordLength = thirdWord3.length;
var fourthWordLength = fourthWord3.length;
var fifthWordLength = fifthWord3.length;

console.log("====Soal 4====");
console.log('First Word: ' + exampleFirstWord3 + ', with length: ' + firstWordLength); 
console.log('Second Word: ' + secondWord3 + ', with length: ' + secondWordLength); 
console.log('Third Word: ' + thirdWord3 + ', with length: ' + thirdWordLength); 
console.log('Fourth Word: ' + fourthWord3 + ', with length: ' + fourthWordLength); 
console.log('Fifth Word: ' + fifthWord3 + ', with length: ' + fifthWordLength);
console.log("");
