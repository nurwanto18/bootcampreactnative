// Soal if-else
var nama = "Jane"
var peran = ""

console.log("===Soal if-else===")
if(nama===""){
    console.log("Nama harus diisi!");
}else if(nama!=="" && peran===""){
    console.log("Halo "+nama+", Pilih peranmu untuk memulai game!");
}else if(peran==="Penyihir"){
    console.log("Selamat datang di Dunia Werewolf, "+nama);
    console.log("Halo "+peran+" "+nama+", kamu dapat melihat siapa yang menjadi werewolf!");
}else if(peran==="Guard"){
    console.log("Selamat datang di Dunia Werewolf, "+nama);
    console.log("Halo "+peran+" "+nama+" kamu akan membantu melindungi temanmu dari serangan werewolf.");
}else if(peran==="Werewolf"){
    console.log("Selamat datang di Dunia Werewolf, "+nama);
    console.log("Halo "+peran+" "+nama+" Kamu akan memakan mangsa setiap malam!");
}
console.log("")


//Soal switch case
var tanggal = 10; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = 11; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 1995; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)

var bulanString = "";

console.log("===Soal switch case===")

switch(bulan){
    case 1  : { bulanString = "Januari"; break;}
    case 2  : { bulanString = "Februari"; break;}
    case 3  : { bulanString = "Maret"; break;}
    case 4  : { bulanString = "April"; break;}
    case 5  : { bulanString = "Mei"; break;}
    case 6  : { bulanString = "Juni"; break;}
    case 7  : { bulanString = "Juli"; break;}
    case 8  : { bulanString = "Agustus"; break;}
    case 9  : { bulanString = "September"; break;}
    case 10  : { bulanString = "Oktober"; break;}
    case 11  : { bulanString = "November"; break;}
    case 12  : { bulanString = "Desember"; break;}
    default: { bulanString = "" }
}

if(bulanString!==""){
    if((tanggal<1 || tanggal>31) || bulanString === "" || (tahun<1900 || tahun>2200)){
        console.log("invalid tanggal")
    }else{
        console.log(tanggal+" "+bulanString+" "+tahun);
    }
}