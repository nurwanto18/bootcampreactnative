import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { NavigationContainer } from '@react-navigation/native'

import LoginPage from '../component/Login'
import HomePage from '../component/Home'
import AboutPage from '../component/About'
import DetailPage from '../component/Detail'

import { Provider } from 'react-redux';
// Import store
import store from '../redux/store'

const Stack = createStackNavigator();
export default function Routes(){
    return(
        <Provider store={store}>
            <NavigationContainer>
                <Stack.Navigator screenOptions={{
                    headerShown: false,
                    gestureEnabled: false,
                    animationEnabled: 'true'
                }}>
                    <Stack.Screen name="LoginPage" component={LoginPage}/>
                    <Stack.Screen name="HomePage" component={HomePage} />
                    <Stack.Screen name="AboutPage" component={AboutPage}/>
                    <Stack.Screen name="DetailPage" component={DetailPage}/>
                </Stack.Navigator>
            </NavigationContainer>
        </Provider>
    )
}