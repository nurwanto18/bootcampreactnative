import React, { useEffect, useState } from 'react'
import { StyleSheet, TextInput, View, Text, Image, TouchableOpacity, Alert } from 'react-native'
import { actionCreators } from '../redux/todoListRedux'
import { connect } from 'react-redux'
//firebase
import * as firebase from 'firebase'
// Import store
import store from '../redux/store'

var firebaseConfig = {
    apiKey: "AIzaSyCspBlt7r37psbDzlramivfQX0iXBGM3o8",
    authDomain: "sanbercodetrain.firebaseapp.com",
    projectId: "sanbercodetrain",
    storageBucket: "sanbercodetrain.appspot.com",
    messagingSenderId: "504888654408",
    appId: "1:504888654408:web:b0c302168b2d8a4dec367d",
    measurementId: "G-K786H5SD7G"
  }; 

if(!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig)
}

const mapStateToProps = (state) => ({
    todos: state.todos,
})

const Login = (props) => {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    

    const onLogin = () => {
        if(email == '' || password == ''){
            Alert.alert('Error', 'email or password is empty')
            return;
        }
        firebase.auth().signInWithEmailAndPassword(email, password)
        .then(()=> {
            const { dispatch } = props
            dispatch(actionCreators.add(email))
            props.navigation.navigate("HomePage")
        }).catch(()=>{
            Alert.alert('Error', 'email or password is wrong')
        })
        
        setEmail("")
        setPassword("")
    }

    return(
        // <Provider store={store}>
            <View style={styles.container}>
                <View style={styles.motto}>
                    <Text style={{fontWeight: 'bold', color: '#FFF', fontSize: 20}}>Check Your Fav Here!</Text>
                </View>
                <Image
                    source={require('../assets/img/logo.png')} 
                    styles={styles.logo}  
                />
                <View style={styles.box}>
                    <TextInput placeholder="email" value={email} style={styles.textInput} onChangeText={(val)=>{setEmail(val)}}/>
                    <TextInput placeholder="password" value={password} style={styles.textInput} onChangeText={(val)=>{setPassword(val)}} secureTextEntry/>
                    <TouchableOpacity style={styles.button} onPress={() => onLogin()}>
                        <Text style={{color: '#fff', fontWeight: 'bold'}}>Login</Text>
                    </TouchableOpacity>
                </View>
            </View>
        // </Provider>
    )
}

export default connect(mapStateToProps)(Login);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#FF0303',
        alignItems: 'center',
        justifyContent: 'center'
    },
    motto: {
        alignItems: 'center',
        alignSelf: 'center'
        
    },
    logo: {
        zIndex: 2,
        width: 100,
        height: 100
    },
    box: {
        zIndex: -1,
        flexDirection: 'column',
        alignItems: 'center',
        backgroundColor: '#fff',
        width: '70%',
        padding: 50,
        marginTop: -100,
        borderRadius: 20,
        paddingTop: 100
    },
    textInput: {
        // borderWidth: 0.2,
        backgroundColor: '#C4C4C4',
        justifyContent: 'center',
        width: 200,
        padding: 10,
        margin: 5,
        borderRadius: 10
    },
    button: {
        marginTop: 10, 
        borderRadius: 10,
        backgroundColor: '#D61E1E',
        padding: 10,
        width: 100,
        alignItems: 'center'
    }
})