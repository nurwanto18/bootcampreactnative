import React, { useEffect, useState } from 'react'
import { StyleSheet, View, Text, Image, TouchableOpacity, ScrollView, FlatList } from 'react-native'
import store from '../redux/store'
import { Drawer, Badge, ActivityIndicator} from '@ant-design/react-native';
import axios from 'axios'
import { connect } from 'react-redux'
import { actionCreators } from '../redux/todoListRedux'
import { BackHandler } from 'react-native';


const mapStateToProps = (state) => ({
    todos: state.todos,
})

const Home = (props) => {
    const [data, setData] = useState(null);
    const [promo, setPromo] = useState(null);
    const state = store.getState();
    const [user, setUser] = useState(null);
    

    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', () => true);
        if(user == null)
            setUser(state.todos[0])

        if(data===null){
            refreshData();
        }

        if(promo == null){
            refreshPromo();
        }

        return () => {
            BackHandler.removeEventListener('hardwareBackPress', () => true);
        };
    })

    const logout = () => {
        const { dispatch } = props
        dispatch(actionCreators.remove(user))

        props.navigation.navigate("LoginPage")
    }

    const refreshPromo = () => {
        axios.get(`https://sanbercodetrain-default-rtdb.firebaseio.com/listpromo.json`)
        .then((response) => {
            var keys = Object.keys(response.data)
            var listData = []
            for(var i=0;i<keys.length;i++){
                var obj = {
                    "id": String(i),
                    "promo": response.data[keys[i]]
                }
                listData.push(obj)
                
            }
            setPromo([...listData])
        }).catch((err)=>{
            console.log(err)
        })
    }

    const refreshData = () => {
        axios.get(`https://sanbercodetrain-default-rtdb.firebaseio.com/resto.json`)
        .then((response) => {
            var keys = Object.keys(response.data)
            var listData = []
            for(let i=0;i<keys.length;i++){
                let keyData = response.data[keys[i]];
                axios.get(`https://sanbercodetrain-default-rtdb.firebaseio.com/${keyData}.json`)
                .then((res)=>{
                    let resdata= res.data
                    listData.push(resdata)
                    
                    // if(i==(listData.length-1)){
                        setData([...listData])
                    // }
                })
                .catch((err)=>{
                    console.log(err);
                })
            }

            
            // setData(listData);
        }).catch((error) => {
            console.error(error);
        }) 
        

    }

    const sidebar = (
      <ScrollView style={styles.sidemenu}>
        <TouchableOpacity style={styles.drawwercard} onPress={() => logout()}>
            <Text style={{fontSize: 18}}>Sign Out</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.drawwercard} onPress={() => navigateDrawwer("HomePage")}>
            <Text style={{fontSize: 18}}>Home</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.drawwercard} onPress={() => navigateDrawwer("AboutPage")}>
            <Text style={{fontSize: 18}}>About Developers</Text>
        </TouchableOpacity>
      </ScrollView>
    );

    const navigateDrawwer = (typenavigate) => {
        if(drawer!=null)
            drawer.closeDrawer()

        props.navigation.navigate(typenavigate)
    }

    const ListPromo = () => {
        return(
        <View style={{flex: 0.2, width: '100%', alignSelf: 'center', marginBottom: 10, padding: 2}}>
            <ActivityIndicator animating={promo===null}/>
            
                <FlatList 
                    data={promo}
                    horizontal={true}
                    keyExtractor={item => item.id}
                    renderItem={({item})=>{
                        return(
                            <View style={{width: 280}}>
                                    {/* <Text>Test</Text> */}
                                    <Image 
                                        style={styles.logopromo}
                                        source={{uri : item.promo}}
                                    />
                            </View>
                        )
                    }}
                />
        </View>
        )
    }

    const ListCard = () => { 
        return(
        <View style={{flex: 0.5, width: '100%', alignSelf: 'center', padding: 10}}>
            <FlatList 
                    data={data}
                    horizontal={false}
                    keyExtractor={item => item.code}
                    contentContainerStyle={{ paddingBottom: 10}}
                    renderItem={({item})=>{
                        return(
                            <View style={{width: '88%', borderBottomWidth: 0.2, alignSelf: 'center', padding: 5}}>
                            <Badge text={"RATE : "+item.rating+"*"} size='small' style={{ flexDirection: 'column', marginTop: 20}}>
                                
                                    <TouchableOpacity style={styles.boxdata} 
                                        onPress={() => props.navigation.navigate("DetailPage",{item})}>
                                        <Image 
                                            style={styles.logorestaurant}
                                            source={{uri : item.image}}
                                        />
                                        <View style={{flexDirection: 'column', marginLeft: 25}}>
                                            <Text style={{fontWeight: 'bold', fontSize: 15}}>{item.name}</Text>
                                            <Text>{item.place}</Text>
                                        </View>
                                    </TouchableOpacity>
                                
                            </Badge>
                            </View>
                        )
                    }}
                />
                
        </View>
    )
}
    return (
      <Drawer
        sidebar={sidebar}
        position="left"
        open={false}
        drawerRef={el => drawer = el}
        onOpenChange={false}
        drawerBackgroundColor="#FFF"
      >
        <View style={styles.container}>
            <View style={{ marginTop: 50, alignItems: 'flex-start', marginLeft: 15 }}>
                <TouchableOpacity onPress={() => drawer && drawer.openDrawer()} style={styles.button}>
                    <Image
                        source={require('../assets/img/menu-sharp.png')}
                    />
                </TouchableOpacity>
            </View>
            <View style={styles.content}>
                <View styles={styles.logo}>
                    <Image
                        source={require('../assets/img/logo.png')}
                    />
                </View>
                <View style={styles.card}>
                    
                    <Text style={{fontWeight: 'bold', fontSize: 18, textAlign: 'center'}}>Hi {user}!</Text>
                    <Text style={{fontWeight: 'bold', fontSize: 15, alignSelf: 'flex-start', paddingLeft: 20, marginTop: 10}}>Your promo here!</Text>
                    {
                        promo != null && ( 
                            <ListPromo />
                        )
                    }
                    <Text style={{fontWeight: 'bold', fontSize: 15, alignSelf: 'flex-start', paddingLeft: 20}}>Find your fav here!</Text>
                    {
                        data != null && ( 
                            <ListCard />
                        )
                    }
                </View>
            </View>
            
        </View>
      </Drawer>
    );
}

export default connect(mapStateToProps)(Home)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FF0303',
        // alignItems: 'center',
        // justifyContent: 'center'
    },
    content: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 10
    },
    card: {
        zIndex: -1,
        width: '95%',
        height: '100%',
        backgroundColor: '#fff',
        marginTop: -100,
        alignItems: 'center',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        paddingTop: 100
    },
    sidemenu: {
        flex: 1,
        marginTop: 80
    },
    logo:{
        width: 100,
        height: 100,
        marginTop: 200
    },
    list: {

    },
    drawwercard: {
        width: '95%',
        padding: 15,
        alignSelf: 'center',
        borderBottomWidth: 0.3,
    },
    menubutton: {
        width: 100,
        height: 100,
        alignSelf: 'center'
    },
    button: {
        // backgroundColor: '#D61E1E'
    },
    boxdata: {
        width: '100%',
        margin: 5,
        // borderWidth: 0.6,
        
        borderRadius: 5,
        alignItems: 'center',
        alignSelf: 'center',
        flexDirection: 'row',
        // padding: 10
    },
    logorestaurant: {
        width: 80,
        height: 80,
        // borderRadius: 50,
        borderWidth: 0.2,
        borderColor: '#c4c4c4'
    },
    logopromo: {
        height: 150,
        width: '100%'
    }
})

