import React, { useEffect } from 'react'
import { View } from 'react-native'
// Import store
import store from '../redux/store'
const Logout = (props,{navigation}) => {
    const state = store.getState();

    const mapStateToProps = (state) => ({
        todos: state.todos,
    })

    useEffect(()=>{
        const { dispatch } = props

        dispatch(actionCreators.remove(state.todos[0]))
    })

    return(
        <View>
            <Text>Will logout...</Text>
        </View>
    )
}

export default (mapStateToProps)(Logout)