import React, { useEffect, useState } from 'react'
import { StyleSheet, View, Text, Image, TouchableOpacity, ScrollView, FlatList } from 'react-native'
import store from '../redux/store'
import { Drawer, Badge} from '@ant-design/react-native';

const Detail = ({route, navigation}) => {
    const {item} = route.params;

    
    return (
        <View style={styles.container}>
            <View style={{ alignItems: 'flex-start', marginLeft: 20, marginTop: 70 }}>
                    <TouchableOpacity onPress={() => navigation.navigate('HomePage')}>
                        <Image
                            source={require('../assets/img/back.png')}
                        />
                    </TouchableOpacity>
                </View>
            <View style={styles.content}>
                <View styles={styles.logo}>
                    <Image
                        source={require('../assets/img/logo.png')}
                    />
                </View>
                <View style={styles.card}>
                    <Text style={{fontSize: 25, fontWeight: 'bold'}}>Profile Restaurant</Text>
                    <Text style={{fontSize: 20}}>{item.name}</Text>
                    <View style={{marginTop: 10, marginBottom: 15}}>
                        <Image
                            style={styles.logorestaurant}
                            source={{uri : item.image}}
                        />
                    </View>
                    <View style={styles.desc}>
                        <Text style={{fontSize: 17, fontWeight: 'bold'}}>Name</Text>
                        <Text>{item.name}</Text>
                    </View>
                    <View style={styles.desc}>
                        <Text style={{fontSize: 17, fontWeight: 'bold'}}>Rating</Text>
                        <Text>{item.rating}/5</Text>
                    </View>
                    <View style={styles.desc}>
                        <Text style={{fontSize: 17, fontWeight: 'bold'}}>Location</Text>
                        <Text>{item.place}</Text>
                    </View>
                    <View style={styles.desc}>
                        <Text style={{fontSize: 17, fontWeight: 'bold'}}>Range</Text>
                        <Text>{item.range}</Text>
                    </View>
                    <View style={styles.desc}>
                        <Text style={{fontSize: 17, fontWeight: 'bold'}}>Review</Text>
                        <Text>{item.review}</Text>
                    </View>
                    <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('HomePage')}>
                            <Text style={{color: '#fff', textAlign: 'center'}}>Back Home</Text>
                    </TouchableOpacity>
                </View>
            </View>
            
        </View>
    );
}

export default Detail

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FF0303',
    },
    content: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    card: {
        zIndex: -1,
        width: '95%',
        height: '100%',
        backgroundColor: '#fff',
        marginTop: -100,
        alignItems: 'center',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        paddingTop: 100
    },
    sidemenu: {
        flex: 1,
        marginTop: 80
    },
    logo:{
        width: 100,
        height: 100,
        marginTop: 200
    },
    list: {

    },
    drawwercard: {
        width: '95%',
        padding: 15,
        alignSelf: 'center',
        borderBottomWidth: 0.3,
    },
    menubutton: {
        width: 100,
        height: 100,
        alignSelf: 'center'
    },
    button: {
        backgroundColor: '#D61E1E',
        marginTop: 20,
        padding: 5,
        width: 100,
        alignItems: 'center',
        borderRadius: 35,
        marginBottom: 20
    },
    boxdata: {
        width: '100%',
        margin: 5,
        // borderWidth: 0.6,
        
        borderRadius: 5,
        alignItems: 'center',
        alignSelf: 'center',
        flexDirection: 'row',
        // padding: 10
    },
    logorestaurant: {
        width: 100,
        height: 100,
        borderRadius: 50,
        borderWidth: 2,
        borderColor: '#c4c4c4',
    },
    logopromo: {
        height: 150,
        width: '100%'
    },
    desc: {
        flexDirection: 'row',
        width: '100%', 
        justifyContent: 'space-between', 
        // borderWidth: 1,
        alignSelf: 'center', 
        alignItems: 'center',
        padding: 15,
        borderBottomWidth: 0.3,
        borderColor: '#c4c4c4'
    }
})

