import React, { useEffect } from 'react'
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native'
import { BackHandler } from 'react-native';

const About = ({navigation}) => {
    useEffect(()=>{
        BackHandler.addEventListener('hardwareBackPress2', () => false);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress2', () => false);
        };
    })
    return(
            <View style={styles.container}>
                <View style={{ alignItems: 'flex-start', marginLeft: 20, marginTop: -20 }}>
                    <TouchableOpacity onPress={() => navigation.navigate('HomePage')}>
                        <Image
                            source={require('../assets/img/back.png')}
                        />
                    </TouchableOpacity>
                </View>
                <View style={styles.card}>
                    <View style={{alignItems: 'center', marginBottom: 10}}>
                        <Text style={{fontSize: 25, fontWeight: 'bold'}}>Contact Us</Text>
                    </View>
                    <View style={styles.content}>
                        <Image
                            source={require('../assets/img/mail.png')} 
                            style={{alignSelf: 'flex-start'}}  
                        />
                        <Text style={{marginLeft: 20, alignSelf: 'center', fontSize: 17}}>prodine@pdine.co.id</Text>
                    </View>
                    <View style={styles.content}>
                        <Image
                            source={require('../assets/img/call.png')} 
                            style={{alignSelf: 'flex-start'}}  
                        />
                        <Text style={{marginLeft: 20, alignSelf: 'center', fontSize: 17}}>+62 851 9999 0123</Text>
                    </View>
                    <View style={styles.content}>
                        <Image
                            source={require('../assets/img/ig.png')} 
                            style={{alignSelf: 'flex-start'}}  
                        />
                        <Text style={{marginLeft: 20, alignSelf: 'center', fontSize: 17}}>prodine.official</Text>
                    </View>
                    <View style={styles.content}>
                        <Image
                            source={require('../assets/img/map.png')} 
                            style={{alignSelf: 'flex-start'}}  
                        />
                        <Text style={{marginLeft: 20, alignSelf: 'center', fontSize: 17, textAlign: 'left', width: 170}}>Tembok Maria Mikasa Street, No 129 Paradis, Yeagerist</Text>
                    </View>
                    <View style={{alignItems: 'center'}}>
                        <Text style={{fontSize: 22, fontWeight: 'bold', marginTop: 30}}>Find Your Fav Here!</Text>
                        <Text style={{fontSize: 15, fontWeight: 'bold'}}>Regards,</Text>
                        <Image
                            source={require('../assets/img/logo.png')} 
                            style={styles.logo}  
                        />
                        <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('HomePage')}>
                            <Text style={{color: '#fff', textAlign: 'center'}}>Back Home</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
    )
}

export default About

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        backgroundColor: '#FF0303',
    },
    button: {
        backgroundColor: '#D61E1E',
        marginTop: 20,
        padding: 5,
        width: 100,
        alignItems: 'center',
        borderRadius: 35,
        marginBottom: 20
    },
    menubutton: {
        flex: 1,
        width: null,
        height: null,
        resizeMode: 'contain'
    },
    drawwercard: {
        width: 100,
        padding: 15,
        alignSelf: 'center',
        borderBottomWidth: 0.3,
    },
    card: {
        justifyContent: 'center',
        flexDirection: 'column',
        width: '90%',
        height: '70%',
        alignSelf: 'center',
        backgroundColor: '#fff',
        marginTop: 50,
        borderRadius: 30
    },
    content: {
        flexDirection: 'row', 
        width: '70%', 
        // justifyContent: 'space-between', 
        alignSelf: 'center', 
        padding: 10
    },
    logo: {
        width: 70,
        height: 70
    }
})