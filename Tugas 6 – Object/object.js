//soal 1
console.log("===soal 1===");
function arrayToObject(arr){
    var now = new Date();
    var thisYear = now.getFullYear();

    if(arr.length===0){
        console.log("//Error Case");
        console.log("\"\"");
        return;
    }

    for(var i=0;i<arr.length;i++){
        var name = arr[i][0]+" "+arr[i][1];
        var gender = arr[i][2];
        var age = arr[i][3] === undefined ? -1 : (thisYear - arr[i][3]);

        var obj = {
            firstName:arr[i][0],
            lastName:arr[i][1],
            gender:gender,
            age: age < 0 ? "Invalid birth year" : age
        }
        console.log((i+1)+". "+name+" : "+JSON.stringify(obj))
    }
    console.log("");
}

// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people)

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2)

// Error case 
arrayToObject([])

console.log("");


//soal 2
console.log("===soal 2===");
function shoppingTime(memberId, money){
    if(memberId === undefined || memberId === '')
        return "Mohon maaf, toko X hanya berlaku untuk member saja";

    if(money < 50000)
        return "Mohon maaf, uang tidak cukup";

    var listSale = [{"barang":"Sepatu Stacattu", "harga":1500000},
                    {"barang":"Baju Zoro", "harga":500000},
                    {"barang":"Baju H&N", "harga":250000},
                    {"barang":"Sweater Uniklooh", "harga":175000},
                    {"barang":"Casing Handphone", "harga":50000},
    ];

    var returnObj = {money};
    
    var listPurchased = [];
    for(var i=0;i<listSale.length;i++){
        if(money >= listSale[i].harga){
            listPurchased.push(listSale[i].barang);
            money-=listSale[i].harga;
        }
    }
    returnObj.listPurchased = listPurchased;
    returnObj.changeMoney = money;

    return returnObj;
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));

console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja
console.log("");


//soal 3
console.log("===soal 3===");
function naikAngkot(listPenumpang){
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    
    if(listPenumpang.length===0){
        return listPenumpang;
    }else{
        var arrReturn = [];
        for(var i=0;i<listPenumpang.length;i++){
            var start = listPenumpang[i][1];
            var finish = listPenumpang[i][2];
            var obj = {
                "penumpang":listPenumpang[i][0],
                "naikDari":start,
                "tujuan":finish
            };
            
            var isStarted = false;
            var harga = 0;
            for(var j=0;j<rute.length;j++){
                if(isStarted){
                    harga+=2000;
                }
                if(rute[j]==start){
                    isStarted=true;
                }else if(rute[j]==finish){
                    break;
                }
            }
            obj.bayar = harga;
            arrReturn.push(obj);
        }
        return arrReturn;
    }
}

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]

console.log("");