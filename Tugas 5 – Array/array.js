//soal 1
console.log("===soal 1===")
//Code di sini
function range(startNum, finishNum){
    if(startNum===undefined || finishNum===undefined)
        return -1;

    var arrReturn = [];
    if(startNum <= finishNum){
        //asc
        for(var i=startNum;i<=finishNum;i++){
            arrReturn.push(i);
        }
    }else{
        //desc
        for(var i=startNum;i>=finishNum;i--){
            arrReturn.push(i);
        }
    }
    return arrReturn;
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 
console.log()


//soal 2
console.log("===soal 2===")
// Code di sini
function rangeWithStep(startNum, finishNum, step){
    if(startNum===undefined || finishNum===undefined)
        return -1;
    
    var arrReturn = [];
    if(startNum <= finishNum){
        //asc
        while(startNum <= finishNum){
            arrReturn.push(startNum);
            startNum+=step;
        }
    }else{
        //desc
        while(startNum >= finishNum){
            arrReturn.push(startNum);
            startNum-=step;
        }
    }
    return arrReturn;
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 
console.log()


//soal 3
console.log("===soal 3===")
// Code di sini
function sum(startNum, finishNum, step = 1){
    var sumReturn = 0;
    if(startNum===undefined)
        return sumReturn;
    
    if(finishNum===undefined)
        return startNum;
    
    
    if(startNum <= finishNum){
        //asc
        while(startNum <= finishNum){
            sumReturn+=startNum;
            startNum+=step;
        }
    }else{
        //desc
        while(startNum >= finishNum){
            sumReturn+=startNum;
            startNum-=step;
        }
    }
    return sumReturn;
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 
console.log()


//soal 4
console.log("===soal 4===")

function dataHandling(arrInput){
    for(var i=0;i<arrInput.length;i++){
        console.log("Nomor ID : "+arrInput[i][0]);
        console.log("Nama Lengkap : "+arrInput[i][1]);
        console.log("TTL : "+arrInput[i][2]+" "+arrInput[i][3]);
        console.log("Hobi : "+arrInput[i][4]);
        console.log();
    }
}

//contoh input
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]
dataHandling(input);
console.log()

//soal 5
console.log("===soal 5===")
// Code di sini
function balikKata(kata){
    var result = "";

    for(var i=(kata.length-1);i>=0;i--){
        result+=kata[i]
    }

    return result;
}
 
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 
console.log()

//soal 6
console.log("===soal 6===")
function dataHandling2(arrInput){
    console.log(arrInput);
    var name = arrInput[1];
    var splitDate = arrInput[3].split("/");
    var bulan = splitDate[1];
    var splitDateJoin = splitDate.join("-");
    var splitDateDesc = splitDate.sort(function (value1, value2) { return value2 - value1 });
    var nameResult = name.slice(0,15);

    //show string
        switch(bulan){
            case "01" : {console.log("Januari"); break;}
            case "02" : {console.log("Februari"); break;}
            case "03" : {console.log("Maret"); break;}
            case "04" : {console.log("April"); break;}
            case "05" : {console.log("Mei"); break;}
            case "06" : {console.log("Juni"); break;}
            case "07" : {console.log("Juli"); break;}
            case "08" : {console.log("Agustus"); break;}
            case "09" : {console.log("September"); break;}
            case "10" : {console.log("Oktober"); break;}
            case "11" : {console.log("November"); break;}
            case "12" : {console.log("Desember"); break;}
            default : {console.log("Undefined")}
        }
        
    //sort desc
    console.log(splitDateDesc);

    //join -
    console.log(splitDateJoin);

    //name 15 char
    console.log(nameResult);
    console.log();
}

var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]
dataHandling2(input);
console.log()