let readBooksPromise = require('./promise.js')
 
let books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
let time = 10000;
let ind = 0;
function readBooks(){
    readBooksPromise(time, books[ind]).then(function (fulfilled) {
        // console.log(fulfilled);
        time = fulfilled;
        ind++;
        if(books[ind]!=undefined)
            readBooks();
    })
    .catch(function (error) {
        console.log(error);
    });
}

readBooks();