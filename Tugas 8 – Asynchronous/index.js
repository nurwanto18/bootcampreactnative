// di index.js
let readBooks = require('./callback.js')
 
let books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
let time = 10000;
let ind = 0;
function readAllBooks (currentTime){
    if(books[ind]!=undefined){
        time = currentTime;
        readBooks(time, books[ind], readAllBooks);
        ind++; 
    }
};
readAllBooks(time);