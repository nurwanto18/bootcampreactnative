// soal 1
var i = 0;
console.log("===soal 1===");

console.log("LOOPING PERTAMA");
while(i<20){
    i = i+2;
    console.log(i + " - I love coding");
}

console.log("LOOPING KEDUA");
while(i>0){
    console.log(i+" - I will become a mobile developer");
    i = i-2;
}

console.log("");

// soal 2
console.log("===soal 2===");
for(var i=1;i<=20;i++){
    if(i%3!=0){
        if(i%2==1){
            console.log(i+" - Santai");
        }else{
            console.log(i+" - Berkualitas");
        }
    }else{
        if(i%2==1){
            console.log(i+" - I Love Coding"); 
        }else{
            console.log(i+" - Berkualitas"); 
        }
    }
}
console.log("");


// soal 3
console.log("===soal 3===");
var pgr = "";
for(var i=0;i<4;i++){
    for(var j=0;j<8;j++){
        pgr=pgr+"#";
    }
    console.log(pgr);
    pgr="";
}
console.log("");


// soal 4
console.log("===soal 4===");
var pgr = "#";
for(var i=0;i<8;i++){
    console.log(pgr);
    pgr=pgr+"#";
}
console.log("");


// soal 5
console.log("===soal 5===");
var pgr = "";
var pgrTemp = "#";
for(var i=0;i<8;i++){
    for(var j=0;j<8;j++){
        if(pgrTemp===" "){
            pgr=pgr+"#";
            pgrTemp="#";
        }else{
            pgr=pgr+" ";
            pgrTemp=" ";
        }
    }
    console.log(pgr);
    if(pgrTemp==="#"){
        pgrTemp=" ";
    }else{
        pgrTemp="#";
    }
    pgr = "";
}
console.log("");