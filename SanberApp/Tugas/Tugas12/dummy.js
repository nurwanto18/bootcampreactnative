const Data = [
    {
        id : "1",
        image: require("./asset/profile1.png"),
        name:'Achmad Hilmy' ,
        message:'Hello Achmad Hilmy',
        time:'12.00',
        totalMessage:'2'
    },
    {
        id : "2",
        image: require("./asset/profile2.png"),
        name:'Deddy Corbuzier' ,
        message:'5 4 3 2 1 Close the door!',
        time:'12.00',
        totalMessage:'4'
    },
    {
        id : "3",
        image: require("./asset/profile3.png"),
        name:'Bobon Santoso' ,
        message:'I\'m the next mastercheft!',
        time:'12.00',
        totalMessage:'1'
    },
    {
        id : "4",
        image: require("./asset/profile4.png"),
        name:'Jess No Limit' ,
        message:'Be your self and never surrender',
        time:'10.30',
        totalMessage:'2'
    },
    {
        id : "5",
        image: require("./asset/profile5.png"),
        name:'Jerome Polin' ,
        message:'Mantappu Jiwaaa',
        time:'08.12',
        totalMessage:'1'
    }
]

export { Data }