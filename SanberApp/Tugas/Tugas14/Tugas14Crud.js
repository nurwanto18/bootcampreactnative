import React, { useEffect, useState } from 'react'
import {StyleSheet, View, FlatList, Text, TextInput, Button, TouchableOpacity, Alert} from 'react-native'
import axios from 'axios';

const Tugas14 = () => {
    const [listItem, setListItem] = useState([]);
    const [titleTemp, setTitleTemp] = useState("");
    const [valueTemp, setValueTemp] = useState("");
    const [isEdit, setIsEdit] = useState(false);
    const [id, setId] = useState(-1);
 
    useEffect(() => {
        if(listItem.length < 1){
            RefreshData()
        }
    })

    const RefreshData = () =>{
        axios.get(`https://achmadhilmy-sanbercode.my.id/api/v1/news`)
        .then(res => {
            let dataResp = res.data
            if(dataResp.data!=null){
                // let arrData = data.data
                var arrData = dataResp.data;
                console.log(arrData)
                setListItem([...arrData])
            }
        })
    }

    const Delete = (idDel) => {
        axios.delete(`https://achmadhilmy-sanbercode.my.id/api/v1/news/${idDel}`)
        .then(() => {
            RefreshData()
        }); 
    }

    const Submit = () => {
        var obj = {
            title: titleTemp,
            value: valueTemp
        }
        console.log(obj);
        if(isEdit){
            axios.put(`https://achmadhilmy-sanbercode.my.id/api/v1/news/${id}`, obj)
            .then((res) => {
                console.log(res)
                setIsEdit(false)
                RefreshData()
            })
        }else{
            axios.post(`https://achmadhilmy-sanbercode.my.id/api/v1/news`, obj)
            .then((res)=>{
                console.log(res.data)
                RefreshData()
            })
        }
        setTitleTemp("")
        setValueTemp("")
        
    }

    const ChangeUpdate = (item) => {
        setId(item.id)
        setIsEdit(true)
        setTitleTemp(item.title)
        setValueTemp(item.value)
    }

    const Card = ({item}) => {
        return(
            <View style={styles.card}>
                <View style={{alignSelf: 'flex-start', width: '65%', padding: 10}}>
                    <Text style={{fontSize: 15, fontWeight: 'bold'}}>{item.title}</Text>
                    <Text>{item.value}</Text>
                </View>
                <View style={styles.buttoncard}>
                    <Button 
                        title="Edit"
                        color="#0a75ad"
                        onPress={() => ChangeUpdate(item)}
                    />
                    <Button 
                        title="Delete"
                        color="#f91010"
                        onPress={() => Delete(item.id)}
                    />
                </View>
            </View>

        );
    }

    return(
        <View style={styles.container}>
            {/* FORM */}
            <View style={styles.form}>
                <View style={styles.forminput}>
                    <Text>Title</Text>
                    <TextInput value={titleTemp} onChangeText={(val)=>{setTitleTemp(val)}} style={{marginLeft: 17, borderBottomWidth: 0.3, paddingHorizontal: 10, width: 250}} placeholder="title"/>
                </View>
                <View style={styles.forminput}>
                    <Text>Value</Text>
                    <TextInput value={valueTemp} onChangeText={(val)=>{setValueTemp(val)}} style={{marginLeft: 10, borderBottomWidth: 0.3, paddingHorizontal: 10, width: 250}} placeholder="value"/>
                </View>
                <Button 
                    title={isEdit ? "Update" : "Add"}
                    onPress={() => Submit()}
                />
            </View>
            {/* LIST */}
            <View style={{marginTop: 30}}>
                {
                listItem!==null && 
                    (<FlatList 
                        data={listItem}
                        keyExtractor={item => item.id.toString()}
                        renderItem={({item})=>{
                            return(
                                <Card item={item}/>
                                // <Text>{item.value}</Text>
                            )
                        }}
                    />)
                }
                
            </View>
        </View>
    )
}

export default Tugas14;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column'
    },
    card: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '90%',
        borderBottomWidth: 0.3,
        marginBottom: 3,
        alignSelf: 'center'
    },
    buttoncard: {
        flexDirection: 'row',
        alignItems: 'flex-end',
        padding: 7
    },
    form: {
        marginTop: 50
    },
    forminput: {
        flexDirection: 'row',
        padding: 10
    }
})