import React from 'react';
import { ImageBackground, StyleSheet, Text, Image, View, TextInput, Button, TouchableOpacity } from 'react-native';

const LoginScreen = ({navigation}) => {
    return(
        <View style={styleLogin.container}>
            <ImageBackground source={require('./asset/background.png')} style={styleLogin.background} >                
            {/* HEADER */}
                <View style={styleLogin.header}>
                    <Image 
                        style={styleLogin.logo}
                        source={require('./asset/LogoGroup.png')}
                    />
                </View>
                {/* FORM */}
                <View style={styleLogin.form}>
                    <Text style={{fontSize: 50, alignSelf: 'center', marginTop: 10}}>Login</Text>
                    <TextInput placeholder="username" style={styleLogin.forminput}/>
                    <TextInput placeholder="password" style={styleLogin.forminput} secureTextEntry={true}/>
                    <View style={{borderRadius: 10, marginTop: 10, width: 100, alignSelf: 'center'}}>
                        <Button 
                            title="Login"
                            color="#686868"
                            style={styleLogin.button}
                            onPress={()=>navigation.navigate("AboutPage")}
                        />
                    </View>
                </View>
                <View style={{alignSelf: 'center', flexDirection: 'row'}}>
                    <Text>Belum punya akun? </Text>
                    <TouchableOpacity>
                        <Text style={{color: '#0695e6'}}>Daftar disini </Text>
                    </TouchableOpacity>
                </View>
                {/* FOOTER */}
                <View style={styleLogin.footer}>
                    <Text>Copyright 2021 </Text>
                    <TouchableOpacity onPress={() => navigation.navigate("AboutPage")}>
                        <Text style={{color: '#0695e6'}}>doubleU{"&"}R Corps</Text>
                    </TouchableOpacity>
                </View>
            </ImageBackground>

        </View>
    );
}

export default LoginScreen;

const styleLogin = StyleSheet.create({
    container:{
        flex: 1,
        flexDirection: "column",
        // alignItems: 'center'
    },
    background: {
        resizeMode: 'cover'
    },
    header: {
        marginTop: 100,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center'
    },
    logo: {
        width: 200,
        height: 100
    },
    form: {
        flexDirection: 'column',
        paddingHorizontal: 20,
        borderWidth: 0.3,
        borderRadius: 10,
        width: 250,
        height: 220,
        backgroundColor: '#FFF',
        alignSelf: 'center',
        marginTop: 100
    },
    footer: {
        marginTop: 180,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        marginBottom: 20
    },
    button: {
        color: "#fff",
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center'
    },
    forminput: {
        borderRadius: 7,
        borderWidth: 1,
        marginTop: 7,
        height: 30,
        paddingHorizontal: 10,
        borderColor: 'grey'
    }
})