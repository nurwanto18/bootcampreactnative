import React from 'react';
import { ImageBackground, StyleSheet, Text, Image, View, TextInput, Button, TouchableOpacity } from 'react-native';

const Box = ({imageUrl}) => {
    return(
        <TouchableOpacity>
            <View style={styleAbout.box}>
                <Image style={styleAbout.logoporto} source={imageUrl}/>
            </View>
        </TouchableOpacity>
    );
}

const AboutScreen = () => {
    return(
        <View style={styleAbout.container}>
            <ImageBackground source={require('./asset/background.png')} style={styleAbout.background} >                
                {/* HEADER */}
                {/* <View style={styleAbout.header}>
                    <TouchableOpacity>
                        <Image 
                            style={styleAbout.logo}
                            source={require('./asset/arrow.png')}
                        />
                    </TouchableOpacity>
                </View> */}
                {/* SUBHEADER ACC */}
                <View style={styleAbout.account}>
                    <Image 
                        style={styleAbout.accountlogo}
                        source={require('./asset/akun.png')}
                    />
                    <Text style={styleAbout.fontaboutme}>About Me</Text>
                    <Text style={{fontSize:20, color: '#dbdbdb', alignSelf: 'center', marginTop: 6}}>doubleU{"&"}R Corps</Text>
                </View>
                <View>
                    <Text style={{fontSize: 20, marginLeft: 10,marginTop: 50, fontWeight: 'bold'}}>Portofolio</Text>
                </View>
                <View style={styleAbout.rowtemplate}>
                    <Image style={{justifyContent: 'center', alignSelf: 'center', marginLeft: 15}} source={require('./asset/left.png')}/>
                    <Box imageUrl={require('./asset/logo-github.png')}/>
                    <Box imageUrl={require('./asset/logo-gitlab.png')}/>
                    <Box imageUrl={require('./asset/logo-linkedin.png')}/>
                    <Image style={{justifyContent: 'center', alignSelf: 'center', marginLeft: 30}} source={require('./asset/right.png')}/>
                </View>
                <View>
                    <Text style={{fontSize: 20, marginLeft: 10, fontWeight: 'bold', marginTop: 20}}>Social Media</Text>
                    <View style={styleAbout.socialmedia}>
                        <TouchableOpacity style={styleAbout.socialmedia}>
                            <Image source={require('./asset/instagram.png')}/>
                            <Text style={{color: '#0695e6'}}> testinguser</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styleAbout.socialmedia}>
                            <Image style={{marginLeft: 50}} source={require('./asset/logo-twitter.png')}/>
                            <Text style={{color: '#0695e6'}}> @testinguser</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                {/* FOOTER */}
                <View style={styleAbout.footer}>
                    <Text>Copyright 2021 </Text>
                    <TouchableOpacity>
                        <Text style={{color: '#0695e6'}}>doubleU{"&"}R Corps</Text>
                    </TouchableOpacity>
                </View>
            </ImageBackground>

        </View>
    );
}

export default AboutScreen;

const styleAbout = StyleSheet.create({
    container:{
        flex: 1,
        flexDirection: "column"
    },
    background: {
        resizeMode: 'cover'
    },
    header: {
        marginTop: 60,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        alignSelf: 'flex-start'
    },
    logo: {
        width: 27,
        height: 20,
        marginLeft: 20
    },
    account: {
        flexDirection: 'column',
        width: 350,
        height: 350,
        paddingTop: 70,
        alignSelf: 'center',
    },
    accountlogo: {
        width: 150,
        height: 150,
        alignSelf: 'center'
    },
    footer: {
        marginTop: 120,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        marginBottom: 20
    },
    fontaboutme: {
        fontSize: 30, 
        alignSelf: 'center', 
        fontWeight: 'bold', 
        color: '#dbdbdb',
        marginTop: 10
    },
    rowtemplate: {
        flexDirection: 'row'
    },
    box: {
        width: 80,
        height: 80,
        backgroundColor: '#dbdbdb',
        marginLeft: 30,
        marginTop: 10
    },
    logoporto:{
        width: 50,
        height: 50,
        marginTop: 15,
        flexDirection:'column',
        alignSelf: 'center'
    },
    socialmedia: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10
    }
})