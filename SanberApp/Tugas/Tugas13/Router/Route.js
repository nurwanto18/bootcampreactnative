import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createDrawerNavigator } from '@react-navigation/drawer'
import { NavigationContainer } from '@react-navigation/native'

import LoginPage from '../LoginScreen'
import AboutPage from '../AboutScreen'

const Stack = createStackNavigator();
export default function Route(){
    return(
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen name="LoginPage" component={LoginPage}/>
                <Stack.Screen name="AboutPage" component={AboutPage}/>
            </Stack.Navigator>
        </NavigationContainer>
    )
}