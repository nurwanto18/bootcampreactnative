import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet } from 'react-native';
import Quiz from './Quiz/test';
// import Login from './Tugas/Tugas15/Pages/Login';
// import AboutScreen from './Tugas/Tugas13/AboutScreen';
// import LoginScreen from './Tugas/Tugas13/LoginScreen';
// import Tugas14 from './Tugas/Tugas14/Tugas14Crud';
// import Telegram from './Tugas/Tugas12/Telegram';
// import Tugas15 from './Tugas/Tugas15/index';
// import Tugas13 from './Tugas/Tugas13/Router/Route'
// import ThisApp from './Quiz3/index'

export default function App() {
  return (
    // <Telegram />
    // <LoginScreen />
    // <AboutScreen/>
    // <Tugas14 />
    // <Login />
    // <Tugas15 />
    // <Tugas13 />
    // <ThisApp />
    <Quiz />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
